package cn.gary.interceptors;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class InterceptorConfigurer implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //判断请求了那个url：/my_addgwc、/my_gwc1、/my_gwc2、/my_gwc3、/usercenter

        UserAuthInterceptor interceptor = new UserAuthInterceptor();
        registry.addInterceptor(interceptor).addPathPatterns("/my_*")
                .addPathPatterns("/usercenter");
    }

}
