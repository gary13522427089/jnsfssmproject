package cn.gary.service;

import cn.gary.dao.TShoppingProductsDao;
import cn.gary.dao.TUserUserDao;
import cn.gary.models.TShoppingProducts;
import cn.gary.models.TUserUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TShoppingProductsService {
    @Autowired
    TShoppingProductsDao dao;

    public List<TShoppingProducts> list(Integer pageIndex, Integer pageSize){
        Integer offset = (pageIndex - 1) * pageSize;
        return dao.list(offset, pageSize);
    }


    public TShoppingProducts detail(Integer pid){
        return dao.detail(pid);
    }

    public int count() {
        return dao.count();
    }

    public int insert(TShoppingProducts product) {
        int result = dao.insert(product);
        return result;
    }

    public int remove(int id) {
        int result = dao.deleteById(id);
        return result;
    }
}
