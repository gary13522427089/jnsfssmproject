package cn.gary.service;

import cn.gary.dao.TOrderItemsDao;
import cn.gary.dao.TOrdersOrdersDao;
import cn.gary.models.TOrdersOrderItems;
import cn.gary.models.TOrdersOrders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TOrderItemsService {
    @Autowired
    TOrderItemsDao dao;

    //生成订单
    public int insert(TOrdersOrderItems orderItem) {
        int result = dao.insert(orderItem);
        return result;
    }

}
