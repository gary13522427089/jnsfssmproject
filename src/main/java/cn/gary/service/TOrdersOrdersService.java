package cn.gary.service;

import cn.gary.dao.TOrderItemsDao;
import cn.gary.dao.TOrdersCartDao;
import cn.gary.dao.TOrdersOrdersDao;
import cn.gary.models.TOrdersCart;
import cn.gary.models.TOrdersOrderItems;
import cn.gary.models.TOrdersOrders;
import cn.gary.models.TUserUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class TOrdersOrdersService {
    @Autowired
    TOrdersOrdersDao ordersDao;
    @Autowired
    TOrdersCartDao cartDao;
    @Autowired
    TOrderItemsDao orderItemsDao;

    //生成订单
    public int insert(TOrdersOrders cart) {
        int result = ordersDao.insert(cart);
        return result;
    }

    //生成订单
    @Transactional
    public void order(TUserUser user, String orderId,String OrderDeliverName,String OrderDeliverAddress,String OrderDeliverZipCode,String OrderDeliverMobile){
        //user信息(表示层可提供)
        //加载购物记录
        List<TOrdersCart> cartItems = cartDao.findByUserId(user.getUserId());
        Double orderTotal = cartDao.totalByUserId(user.getUserId());

        try{
            //生成 订单表中的订单记录
            TOrdersOrders order = new TOrdersOrders();

            order.setOrderId(orderId);                     //S00000222220210...  主键：唯一性的要求
            order.setOrderSumPrice(orderTotal);
            order.setOrderDeliverName(OrderDeliverName);
            order.setOrderDeliverAddress(OrderDeliverAddress);
            order.setOrderDeliverZipCode(OrderDeliverZipCode);
            order.setOrderDeliverMobile(OrderDeliverMobile);
            //order.setDeliverId();
            //order.setPaymentName();
            //order.setOrderConsignmentNumber();
            //order.setOrderDescribe();
            order.setOrderState("未付款");
            order.setOrderDeleted("0");
            order.setUserId(user.getUserId());
            order.setUserLoginName(user.getUserName());
            DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            order.setOrderBookedDate(dataFormat.format(new Date()));
            //order.setOrderModifyDate();
            ordersDao.insert(order);

            //生成 订单明细表，将购物记录复制到订单明细表中
            for (TOrdersCart cartItem : cartItems){
                //insert...
                TOrdersOrderItems orderItem = new TOrdersOrderItems();
                orderItem.setOrderId(orderId);
                orderItem.setOrderItemProductId(cartItem.getProductId());
                orderItem.setOrderItemProductName(cartItem.getProductName());
                orderItem.setOrderItemAmount(cartItem.getCartAmount());
                orderItem.setOrderItemProductPrice(cartItem.getProductPrice());
                orderItemsDao.insert(orderItem);
            }
            //清除购物表中的购物记录
            cartDao.deleteByUserId(user.getUserId());
            //for (TOrdersCart cartItem : cartItems){
            //delete...
            //cartDao.deleteById(cartItem.getCartId());
            //}
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("下单失败，触发回滚...");
            throw new RuntimeException("数据操作异常，触发回滚，请重新下载...");
            //return false;
        }
    }

}
