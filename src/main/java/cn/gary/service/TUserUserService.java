package cn.gary.service;

import cn.gary.dao.TUserUserDao;
import cn.gary.models.TUserUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

//集成计算类代码、负责为表示层来调用dao层
@Service                        //将当前类纳入spring管理，实例化入容器
public class TUserUserService {

    @Autowired                  //向Spring Container索要
    TUserUserDao dao;

    public List<TUserUser> find() {
        List<TUserUser> users = dao.find();
        return users;
    }

    public TUserUser findByUserName(String username) {
        TUserUser user = dao.findByUserName(username);
        return user;
    }

    public List<TUserUser> findPage(Integer pageIndex, Integer pageSize) {
        Integer offset = (pageIndex - 1) * pageSize;
        List<TUserUser> users = dao.findPage(offset, pageSize);
        return users;
    }

    public int count() {
        return dao.count();
    }

    public int insert(TUserUser user) {
        int result = dao.insert(user);
        return result;
    }

    public TUserUser findById(Integer id) {
        TUserUser user = dao.findById(id);
        return user;
    }

    //修改
    public int update(TUserUser user) {
        int result = dao.insert(user);
        return result;
    }

    public int remove(int id) {
        int result = dao.deleteById(id);
        return result;
    }

    public boolean login(String username, String userpwd) {
        int result = dao.login(username, userpwd);
        return (result > 0) ? true : false;
    }

}
