package cn.gary.service;

import cn.gary.dao.TOrdersCartDao;
import cn.gary.dao.TShoppingProductsDao;
import cn.gary.models.TOrdersCart;
import cn.gary.models.TShoppingProducts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TOrdersCartService {
    @Autowired
    TOrdersCartDao dao;

    public List<TOrdersCart> list(){
        return dao.list();
    }

    public List<TOrdersCart> findByUserId(Integer userId){
        return dao.findByUserId(userId);
    }

    public Double totalByUserId(Integer userId){
        return dao.totalByUserId(userId);
    }

    public int insert(TOrdersCart cart) {
        int result = dao.insert(cart);
        return result;
    }

    public int remove(int cartid) {
        int result = dao.deleteById(cartid);
        return result;
    }
}
