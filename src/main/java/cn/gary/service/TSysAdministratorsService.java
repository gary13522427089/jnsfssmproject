package cn.gary.service;

import cn.gary.dao.TSysAdministratorsDao;
import cn.gary.models.TSysAdministrators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TSysAdministratorsService {
    @Autowired
    TSysAdministratorsDao dao;

    public TSysAdministrators login(String loginName, String loginPwd){
        TSysAdministrators admin = dao.findByUserNameAndUserPwd(loginName, loginPwd);
        //return (admin！=null)?true:false;
        return admin;
    }

}
