package cn.gary.syscontrollers;

import cn.gary.models.TUserUser;
import cn.gary.service.TUserUserService;
import org.omg.CORBA.portable.OutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/sys/user")
public class SysUserController {
    //spring的价值

    @Autowired
    TUserUserService service;

    @GetMapping("/list")        ///user/list
    public ModelAndView list(Integer pageIndex, Integer pageSize, ModelAndView mav) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = 3;
        }

        //表示层先加载数据(显示 所有数据)
        //List<TUserUser> items = service.find();
        List<TUserUser> items = service.findPage(pageIndex, pageSize);

        //求页码（页面分页组件效果）
        int itemsCount = service.count();
        int pageTotal;
        int currentPageIndex = pageIndex;
        int prevPageIndex = (pageIndex - 1==0)?pageIndex:pageIndex - 1;
        int nextPageIndex = pageIndex + 1;
        if (itemsCount % pageSize == 0) {
            pageTotal = (itemsCount / pageSize);
        } else {
            pageTotal = (itemsCount / pageSize) + 1;
        }
        mav.addObject("pageTotal", pageTotal);
        mav.addObject("prevPageIndex", prevPageIndex);
        mav.addObject("nextPageIndex", nextPageIndex);
        mav.addObject("pageSize", pageSize);

        //再将数据，传递至视图，加载视图
        mav.addObject("items", items);
        mav.setViewName("sys/user/list");
        return mav;
        //return "sys/user/list";
    }

    //显示添加用户的页面
    @GetMapping("/add")
    public String add() {
        return "sys/user/add";
    }


    String allowFileType = ".jpg, .jpeg, .gif, .png...";

    //接收数据
    @PostMapping("/add")
    //数据应该根据客户端的数据name名称一一获取，但SpringBoot是高级框架，针对这种情况有更优的选择
    //public String add(String userName, String userPwd, String userPhone, String...){
    public String add(TUserUser user, @RequestParam("userPicturefile") MultipartFile file, HttpServletResponse response) throws IOException {
        //处理上传的文件
        if(!file.isEmpty()){
            //有文件
            //file.getBytes()         //文件大小
            //file.getContentType()   //文件类型
            //file.getInputStream()   //文件二进制流
            //file.getName()          //客户端控件名
            //file.getOriginalFilename()//客户端文件全名
            //文件类型判断
            String fileType = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
            if(allowFileType.indexOf(fileType) > -1){
                //允许上传
            }else{
                //非法文件（登录失败时...）
                response.setCharacterEncoding("utf-8");
                response.setContentType("text/html;charset='utf-8';");
                String scriptinfo = "<script>alert(\"上传文件类型，不正确，只能上传如下格式\\r\\n.jpg, .jpeg, .gif, .png...\");window.location.href='list';</script>";
                //将数据直接发送到客户端浏览器
                response.getWriter().print(scriptinfo);
                return null;
            }
            String filename = UUID.randomUUID().toString().replaceAll("-", "");
            String savePath = "C:\\Users\\Gary\\ProjectDemo\\images\\" + filename + fileType;
            InputStream inputStream = file.getInputStream();
            java.io.OutputStream outputStream = new FileOutputStream(savePath);
            int position = -1;
            byte[] buffer = new byte[1024];
            while((position = inputStream.read(buffer)) != -1){
                outputStream.write(buffer);
            }
            inputStream.close();
            outputStream.close();
            user.setUserPicture(filename + fileType);
        }
        //客户的表单  user
        int result = service.insert(user);
        if (result > 0) {
            //添加成功
            //return "redirect:add";
            return "redirect:list";
        } else {
            //失败，再次显示 添加页，检查数据
            return "sys/user/add";
        }
    }

    @GetMapping("/remove")
    public String remove(Integer id) {
        int result = service.remove(id);
        return "redirect:list";
        //if(result > 0){ }
    }

    @GetMapping("/modify")
    public String modify(Integer id, Model model) {
        //加载数据，显示至页面
        TUserUser user = service.findById(id);

        model.addAttribute("user", user);
        return "sys/user/modify";
        //if(result > 0){ }
    }

    @PostMapping("/modify")
    public String modify(TUserUser user, @RequestParam("userPicturefile") MultipartFile file, HttpServletResponse response, Model model) throws IOException {
        //处理上传的文件
        if(!file.isEmpty()){
            String fileType = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
            if(allowFileType.indexOf(fileType) > -1){
            }else{
                response.setCharacterEncoding("utf-8");
                response.setContentType("text/html;charset='utf-8';");
                String scriptinfo = "<script>alert(\"上传文件类型，不正确，只能上传如下格式\\r\\n.jpg, .jpeg, .gif, .png...\");window.location.href='list';</script>";
                //将数据直接发送到客户端浏览器
                response.getWriter().print(scriptinfo);
                return null;
            }
            String filename = UUID.randomUUID().toString().replaceAll("-", "");
            String savePath = "C:\\Users\\Gary\\ProjectDemo\\images\\" + filename + fileType;
            InputStream inputStream = file.getInputStream();
            java.io.OutputStream outputStream = new FileOutputStream(savePath);
            int position = -1;
            byte[] buffer = new byte[1024];
            while((position = inputStream.read(buffer)) != -1){
                outputStream.write(buffer);
            }
            inputStream.close();
            outputStream.close();
            user.setUserPicture(filename + fileType);
        }
        //客户的表单  user
        int result = service.update(user);
        if (result > 0) {
            return "redirect:list";
        } else {
            //失败，再次显示 添加页，检查数据
            model.addAttribute("user", user);
            return "sys/user/modify";
        }
    }
}
