package cn.gary.dao;

import cn.gary.models.TSysAdministrators;
import cn.gary.models.TUserUser;

import java.util.List;

public interface TSysAdministratorsDao {

    List<TSysAdministrators> find();
    TSysAdministrators findById(Integer id);
    TSysAdministrators findByUserNameAndUserPwd(String loginName,  String loginPwd);
    int deleteById(Integer id);
    int insert(TUserUser user);
    int update(TUserUser user);

}
