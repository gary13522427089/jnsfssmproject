package cn.gary.dao;

import cn.gary.models.TOrdersOrderItems;
import cn.gary.models.TOrdersOrders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据库访问层
 */
@Mapper
public interface TOrderItemsDao {
    @Insert("insert into T_Orders_OrderItems(OrderId,OrderItemProductId,OrderItemProductName,OrderItemAmount,OrderItemProductPrice) values(#{orderId},#{orderItemProductId},#{orderItemProductName},#{orderItemAmount},#{orderItemProductPrice})")
    int insert(TOrdersOrderItems orderItem);
}
