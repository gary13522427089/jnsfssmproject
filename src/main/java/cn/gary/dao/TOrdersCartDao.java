package cn.gary.dao;

import cn.gary.models.TOrdersCart;
import cn.gary.models.TShoppingProducts;

import java.util.List;

/**
 * 数据库访问层
 */
public interface TOrdersCartDao {
    List<TOrdersCart> list();
    List<TOrdersCart> findByUserId(Integer userId);
    Double totalByUserId(Integer userId);
    int deleteById(Integer id);
    int deleteByUserId(Integer userId);
    int insert(TOrdersCart product);
}
