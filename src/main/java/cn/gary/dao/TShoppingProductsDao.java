package cn.gary.dao;

import cn.gary.models.TShoppingProducts;
import cn.gary.models.TUserUser;

import java.util.List;

/**
 * 数据库访问层
 */
public interface TShoppingProductsDao {
    //find、findAll、select、add、insert
    List<TShoppingProducts> list(Integer offset, Integer length);
    int count();
    TShoppingProducts detail(Integer pid);
    int deleteById(Integer id);
    int insert(TShoppingProducts product);
}
