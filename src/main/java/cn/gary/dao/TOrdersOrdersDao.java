package cn.gary.dao;

import cn.gary.models.TOrdersCart;
import cn.gary.models.TOrdersOrders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据库访问层
 */
public interface TOrdersOrdersDao {
    int insert(TOrdersOrders order);
}
