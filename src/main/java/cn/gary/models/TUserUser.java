package cn.gary.models;

import org.apache.ibatis.annotations.ConstructorArgs;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * T_User_User 实体类
 */
public class TUserUser {
    //属性封装
    private Integer userId;
    private String userName;
    private String userPwd;
    private String userPhone;
    private Double userPirce;
    private Integer userSex;
    private String userPicture;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date userBirthday;
    private String userAddress;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Double getUserPirce() {
        return userPirce;
    }

    public void setUserPirce(Double userPirce) {
        this.userPirce = userPirce;
    }

    public Integer getUserSex() {
        return userSex;
    }

    public void setUserSex(Integer userSex) {
        this.userSex = userSex;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }
}
