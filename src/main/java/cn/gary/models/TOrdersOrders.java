package cn.gary.models;

import java.util.Date;
import java.util.UUID;

public class TOrdersOrders {
    private String orderId;
    private Double orderSumPrice;
    private String orderDeliverName;
    private String orderDeliverAddress;
    private String orderDeliverZipCode;
    private String orderDeliverMobile;
    private Integer deliverId;
    private Integer paymentName;
    private String orderConsignmentNumber;
    private String orderDescribe;
    private String orderState;
    private String orderDeleted;
    private Integer userId;
    private String userLoginName;
    private String orderBookedDate;
    private String orderModifyDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getOrderSumPrice() {
        return orderSumPrice;
    }

    public void setOrderSumPrice(Double orderSumPrice) {
        this.orderSumPrice = orderSumPrice;
    }

    public String getOrderDeliverName() {
        return orderDeliverName;
    }

    public void setOrderDeliverName(String orderDeliverName) {
        this.orderDeliverName = orderDeliverName;
    }

    public String getOrderDeliverAddress() {
        return orderDeliverAddress;
    }

    public void setOrderDeliverAddress(String orderDeliverAddress) {
        this.orderDeliverAddress = orderDeliverAddress;
    }

    public String getOrderDeliverZipCode() {
        return orderDeliverZipCode;
    }

    public void setOrderDeliverZipCode(String orderDeliverZipCode) {
        this.orderDeliverZipCode = orderDeliverZipCode;
    }

    public String getOrderDeliverMobile() {
        return orderDeliverMobile;
    }

    public void setOrderDeliverMobile(String orderDeliverMobile) {
        this.orderDeliverMobile = orderDeliverMobile;
    }

    public Integer getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(Integer deliverId) {
        this.deliverId = deliverId;
    }

    public Integer getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(Integer paymentName) {
        this.paymentName = paymentName;
    }

    public String getOrderConsignmentNumber() {
        return orderConsignmentNumber;
    }

    public void setOrderConsignmentNumber(String orderConsignmentNumber) {
        this.orderConsignmentNumber = orderConsignmentNumber;
    }

    public String getOrderDescribe() {
        return orderDescribe;
    }

    public void setOrderDescribe(String orderDescribe) {
        this.orderDescribe = orderDescribe;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getOrderDeleted() {
        return orderDeleted;
    }

    public void setOrderDeleted(String orderDeleted) {
        this.orderDeleted = orderDeleted;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName;
    }

    public String getOrderBookedDate() {
        return orderBookedDate;
    }

    public void setOrderBookedDate(String orderBookedDate) {
        this.orderBookedDate = orderBookedDate;
    }

    public String getOrderModifyDate() {
        return orderModifyDate;
    }

    public void setOrderModifyDate(String orderModifyDate) {
        this.orderModifyDate = orderModifyDate;
    }
}
