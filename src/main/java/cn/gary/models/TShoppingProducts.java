package cn.gary.models;

public class TShoppingProducts {
    private Integer productId;
    private String productName;
    private Integer productClassId;
    private String productSmallPhoto;
    private String productBigPhoto;
    private String productSummary;
    private String productDescribe;
    private Double productMarketPrice;
    private Double productMemberPrice;
    private Integer productStock;
    private Integer productBuyTimes;
    private Integer productHits;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductClassId() {
        return productClassId;
    }

    public void setProductClassId(Integer productClassId) {
        this.productClassId = productClassId;
    }

    public String getProductSmallPhoto() {
        return productSmallPhoto;
    }

    public void setProductSmallPhoto(String productSmallPhoto) {
        this.productSmallPhoto = productSmallPhoto;
    }

    public String getProductBigPhoto() {
        return productBigPhoto;
    }

    public void setProductBigPhoto(String productBigPhoto) {
        this.productBigPhoto = productBigPhoto;
    }

    public String getProductSummary() {
        return productSummary;
    }

    public void setProductSummary(String productSummary) {
        this.productSummary = productSummary;
    }

    public String getProductDescribe() {
        return productDescribe;
    }

    public void setProductDescribe(String productDescribe) {
        this.productDescribe = productDescribe;
    }

    public Double getProductMarketPrice() {
        return productMarketPrice;
    }

    public void setProductMarketPrice(Double productMarketPrice) {
        this.productMarketPrice = productMarketPrice;
    }

    public Double getProductMemberPrice() {
        return productMemberPrice;
    }

    public void setProductMemberPrice(Double productMemberPrice) {
        this.productMemberPrice = productMemberPrice;
    }

    public Integer getProductStock() {
        return productStock;
    }

    public void setProductStock(Integer productStock) {
        this.productStock = productStock;
    }

    public Integer getProductBuyTimes() {
        return productBuyTimes;
    }

    public void setProductBuyTimes(Integer productBuyTimes) {
        this.productBuyTimes = productBuyTimes;
    }

    public Integer getProductHits() {
        return productHits;
    }

    public void setProductHits(Integer productHits) {
        this.productHits = productHits;
    }
}
