package cn.gary.models;

public class TOrdersOrderItems {
    private Integer orderItemId;
    private String orderId;
    private Integer orderItemProductId;
    private String orderItemProductName;
    private Integer orderItemAmount;
    private Double orderItemProductPrice;

    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderItemProductId() {
        return orderItemProductId;
    }

    public void setOrderItemProductId(Integer orderItemProductId) {
        this.orderItemProductId = orderItemProductId;
    }

    public String getOrderItemProductName() {
        return orderItemProductName;
    }

    public void setOrderItemProductName(String orderItemProductName) {
        this.orderItemProductName = orderItemProductName;
    }

    public Integer getOrderItemAmount() {
        return orderItemAmount;
    }

    public void setOrderItemAmount(Integer orderItemAmount) {
        this.orderItemAmount = orderItemAmount;
    }

    public Double getOrderItemProductPrice() {
        return orderItemProductPrice;
    }

    public void setOrderItemProductPrice(Double orderItemProductPrice) {
        this.orderItemProductPrice = orderItemProductPrice;
    }
}
