package cn.gary.controllers;

import cn.gary.models.TShoppingProducts;
import cn.gary.service.TShoppingProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class ProductController {
    @Autowired
    TShoppingProductsService service;

    @GetMapping("/productlist")
    public String list(Integer pageIndex, Integer pageSize, Model model){
        if(pageIndex==null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }

        //加载数据
        List<TShoppingProducts> items = service.list(pageIndex, pageSize);

        model.addAttribute("items", items);

        //加载视图
        return "productlist.html";
    }


    @GetMapping("/productdetail")
    public String productdetail(Integer pid, Model model){

        TShoppingProducts product = service.detail(pid);
        model.addAttribute("product", product);

        return "productdetail";}
}
