package cn.gary.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 只是一个普通 java类，怎么能成为网页
 * 如果成为网页，那需要有url地址
 */
@Controller
public class NewsController {

    @RequestMapping("/newslist")
    public String newslist(){
        System.out.println("控制 器被调用");
        //控制器代码
        //要不要有视图view
        return "newslist";
    }
    @RequestMapping("/detail")
    public void detail(){}



}
